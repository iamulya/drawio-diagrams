<mxfile host="app.diagrams.net" modified="2021-01-16T18:13:41.275Z" agent="5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36" etag="P-WvcxseK8Z_jRdfjgoZ" version="14.2.4" type="gitlab">
  <diagram id="7PiOc-rt5Js6lruVHMRd" name="Page-1">
    <mxGraphModel dx="946" dy="614" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="850" pageHeight="1100" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-9" value="" style="group" vertex="1" connectable="0" parent="1">
          <mxGeometry x="280" y="50" width="330" height="270" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-2" value="" style="group" vertex="1" connectable="0" parent="PwKNdzmz-twX-Tcg-dkE-9">
          <mxGeometry y="50" width="330" height="220" as="geometry" />
        </mxCell>
        <mxCell id="ylar7ApnCoyT4dR1FVpl-1" value="&lt;b&gt;Route 53&lt;/b&gt; is a DNS service. It lets you translate domain&lt;br&gt;names to IP addresses. There’s nothing particularly&lt;br&gt;special about Route 53’s DNS capabilities. In fact, it has a&lt;br&gt;few annoying (but mostly minor) limitations, such as the&lt;br&gt;&lt;b&gt;lack of support for ALIAS records (unless they point to&lt;br&gt;AWS resources) and the lack of DNSSEC support&lt;/b&gt;." style="text;whiteSpace=wrap;html=1;" parent="PwKNdzmz-twX-Tcg-dkE-2" vertex="1">
          <mxGeometry width="330" height="100" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-1" value="&lt;b&gt;It integrates very well with ELB&lt;/b&gt;. There is a significant benefit in having&lt;br&gt;&lt;b&gt;CloudFormation automatically set up your load balancer&lt;br&gt;together with the DNS records for your custom domain&lt;/b&gt;.&lt;br&gt;Route 53 makes this possible, whereas if you were to use&lt;br&gt;a different DNS provider, you’d likely have to manage your&lt;br&gt;DNS records manually." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-2">
          <mxGeometry y="110" width="330" height="110" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-3" value="&lt;b&gt;&lt;font style=&quot;font-size: 15px&quot;&gt;Route53&lt;/font&gt;&lt;/b&gt;" style="text;html=1;strokeColor=none;fillColor=none;align=center;verticalAlign=middle;whiteSpace=wrap;rounded=0;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-9">
          <mxGeometry x="120" width="40" height="40" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-12" value="" style="group" vertex="1" connectable="0" parent="1">
          <mxGeometry x="40" y="360" width="330" height="480" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-10" value="" style="group" vertex="1" connectable="0" parent="PwKNdzmz-twX-Tcg-dkE-12">
          <mxGeometry width="330" height="370" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-5" value="&lt;b&gt;&lt;font style=&quot;font-size: 15px&quot;&gt;SQS&lt;/font&gt;&lt;/b&gt;" style="text;whiteSpace=wrap;html=1;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-10">
          <mxGeometry x="120" width="50" height="30" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-7" value="&lt;b&gt;SQS&lt;/b&gt; is a &lt;b&gt;highly-durable queue&lt;/b&gt; in the cloud. You put&lt;br&gt;messages on one end, and a consumer takes them out&lt;br&gt;from the other side. The messages are consumed in&lt;br&gt;&lt;b&gt;almost first-in-first-out order&lt;/b&gt;, but the &lt;b&gt;ordering is not&lt;br&gt;&lt;br&gt;strict&lt;/b&gt;. The lack of strict ordering happens because your&lt;br&gt;&lt;b&gt;SQS queue is actually a bunch of queues behind the&lt;br&gt;scenes. When you enqueue a message, it goes to a&lt;br&gt;random queue, and when you poll, you also poll a random queue.&lt;/b&gt; In addition, &lt;b&gt;duplicate messages can emerge&lt;/b&gt; within SQS, so your consumers should be prepared to handle this situation." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-10">
          <mxGeometry y="40" width="330" height="180" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-8" value=" SQS is one of the few AWS services that requires&lt;br&gt;&lt;b&gt;zero capacity management&lt;/b&gt;. There is&lt;b&gt; no limit on the rate&lt;br&gt;of messages enqueued or consumed&lt;/b&gt;, and you don’t have&lt;br&gt;to worry about any throttling limits. The number of&lt;br&gt;messages stored in SQS (the backlog size) is also&lt;br&gt;&lt;b&gt;unlimited&lt;/b&gt;. As long as you can tolerate the lack of strict&lt;br&gt;ordering and the possibility of duplicates, this property&lt;br&gt;makes SQS a great default choice for dispatching&lt;br&gt;asynchronous work." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-10">
          <mxGeometry y="230" width="330" height="140" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-11" value="If you really need strict ordering and exactly-once&lt;br&gt;delivery (no duplicates), &lt;b&gt;SQS has an option to enable this&lt;br&gt;property by marking your queue as FIFO&lt;/b&gt;. But these FIFO&lt;br&gt;queues come with a &lt;b&gt;throughput limit of 300 messages per&lt;br&gt;second&lt;/b&gt;, so they’re only viable if you’re certain that your&lt;br&gt;message rate will remain well clear of that limit." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="PwKNdzmz-twX-Tcg-dkE-12">
          <mxGeometry y="380" width="330" height="100" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-13" value="&lt;b&gt;&lt;font style=&quot;font-size: 15px&quot;&gt;Kinesis&lt;/font&gt;&lt;/b&gt;" style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="630" y="360" width="60" height="30" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-14" value="Kinesis stream can be thought of as a &lt;b&gt;highly-durable linked list&lt;/b&gt; in the cloud" style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="555" y="400" width="210" height="40" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-15" value="The main difference between&lt;br&gt;the two services is that SQS can only have &lt;b&gt;one consumer,&lt;br&gt;while Kinesis can have many&lt;/b&gt;. Once an SQS message gets&lt;br&gt;consumed, it gets &lt;b&gt;deleted&lt;/b&gt; from the queue. But Kinesis&lt;br&gt;records get added to a list in a stable order, and any&lt;br&gt;number of consumers can read a copy of the stream by&lt;br&gt;keeping a cursor over this never-ending list." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="555" y="580" width="340" height="110" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-16" value="In addition to supporting multiple consumers, another&lt;br&gt;benefit of Kinesis over SQS is that it &lt;b&gt;can be a lot cheaper&lt;/b&gt;.&lt;br&gt;For example, putting 1&amp;nbsp;KB messages in SQS at an average&lt;br&gt;rate of 500 messages per second will cost you $34.56 per&lt;br&gt;day. A Kinesis stream with 50% capacity headroom can&lt;br&gt;handle that same volume for just $0.96 per day. So there&lt;br&gt;can be about a massive difference in cost." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="555" y="830" width="340" height="110" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-17" value="Kinesis streams&lt;br&gt;are optimized for &lt;b&gt;sequential reads and sequential writes&lt;/b&gt;.&lt;br&gt;Records get added to the end of a file, and reads always&lt;br&gt;happen sequentially from a pointer on that file. Unlike&lt;br&gt;SQS, records in a Kinesis stream &lt;b&gt;don’t get deleted&lt;/b&gt; when&lt;br&gt;consumed, so it’s a pure &lt;b&gt;append-only data structure&lt;/b&gt;&lt;br&gt;behind the scenes. Data simply &lt;b&gt;ages out&lt;/b&gt; of a Kinesis&lt;br&gt;stream once it exceeds its retention period, which is &lt;b&gt;24&lt;br&gt;hours&lt;/b&gt; by default." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="555" y="440" width="330" height="140" as="geometry" />
        </mxCell>
        <mxCell id="PwKNdzmz-twX-Tcg-dkE-18" value="Kinesis is &lt;b&gt;not as easy to use as SQS&lt;/b&gt;.&lt;br&gt;While with SQS you can simply enqueue as many&lt;br&gt;messages as you want without having to worry about&lt;br&gt;capacity or throttling limits, a Kinesis stream is made up&lt;br&gt;of &lt;b&gt;slices of capacity called shards, and it’s up to you to&lt;br&gt;figure out how many shards you need, monitor shard&lt;br&gt;utilization, add shards, and figure out the best way to&lt;br&gt;route records to shards so that they get approximately an equivalent amount of traffic&lt;/b&gt;." style="text;whiteSpace=wrap;html=1;" vertex="1" parent="1">
          <mxGeometry x="555" y="690" width="330" height="140" as="geometry" />
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
